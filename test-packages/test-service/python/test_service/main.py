# -*- mode: python; python-indent: 4 -*-
import ncs
from ncs.application import Service, NanoService


class TestService(Service):
    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        self.log.info('Service create(service=', service._path, ')')

        root.simple_service.create(service.name)
        root.complex_service.create(service.name)


class SimpleService(Service):
    @Service.pre_modification
    def cb_pre_modification(self, tctx, op, kp, root, proplist):
        if op == ncs.dp.NCS_SERVICE_CREATE:
            proplist.append(('key', 'value'))
        return proplist

    @Service.create
    def cb_create(self, tctx, root, service, proplist):
        if not proplist:
            raise ValueError('proplist is empty')
        if len(proplist) != 1 and proplist[0] != ('key', 'value'):
            raise ValueError(f'proplist contains unexpected values: {proplist}')
        self.log.info('Service create(service=', service._path, ')')
        self.log.debug(f'proplist = {proplist}')
        return proplist


class ComplexInit(NanoService):
    @NanoService.create
    def cb_nano_create(self, tctx, root, service, plan, component, state, proplist, component_proplist):
        proplist = [('service_kp', service._path)]
        return proplist


class Main(ncs.application.Application):
    def setup(self):
        self.register_service('test-service-servicepoint', TestService)
        self.register_service('simple-service-servicepoint', SimpleService)
        self.register_nano_service('complex-service-servicepoint', 'test-service:standalone', 'ncs:init', ComplexInit)
