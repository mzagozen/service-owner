# service-owner

*service-owner* is an NSO package that sets the owner of service instances in a stacked service model to a pre-defined user.

NSO administrators can set up NACM rules for authorizing data access to users
and applications. The default behavior in relation to services is such that NACM
rules are not enforced for access operations performed from service callbacks.
In practice this means a service is a controlled way to provide limited access
to devices to a group of users that would otherwise not be allowed to apply
arbitrary changes on the devices. This behavior also includes other services
created as part of a stacked service architecture.

Each transaction is associated with a user and a context. These determine how
the authorization rules configured with NACM are applied. A service instance is
"owned" by a user that last touched (made any changes to) it. For nano-services
this is used when the 'reactive-re-deploy' action progresses the plan. Here lies
the problem - if a nano-service is created as part of a service stack, it will
initially be owned by the user that created the top-level service. If the user
has no access to the lower levels of the stack the nano-service may become stuck
because a reactive-re-deploy runs the plan in the context of the owner. To solve
this, this package has two ways of setting the service-owner:
1. Reset the owner for all services further down the stack from the top-level
   service when the top-level service is touched.
2. Reset the owner for a nano-service itself in one of the early states.

# Usage

The service-owner global configuration would typically be part of NSO initial
configuration.

## Reset the owner for a service stack

In the testenv we use a service stack with the `/test-service` on
top. The top service creates `/simple-service` and `/complex-service` which is a
nano-service. To set the owner for lower levels of the stack we create the
service-owner profile:

```
service-owner test-service {
    username    private;
    context     internal;
    entry-point [ /test-service:test-service ];
}
```

The `entry-point` leaf-list may contain multiple top-level services if the other
profile settings can be shared.

When a top-level service instance is modified the service-owner package will
iterate all the services in the stack and reset the owner where needed. This is
done with a kicker, so there exists a short time window where the originating
user is still the owner of the services in the stack. Please keep this in mind
when creating your service stack to avoid race conditions.

## Reset the owner for a standalone nano-service

One of the race conditions we found when using this packages was when a
nano-service is included in the service stack. The nano-service plan has two
properties:
1. The very first state in the plan includes `ncs:force-commit`.
2. The second state in the plan is guarded with a pre-condition.

If the pre-condition is already met at the time the service instance is created
the nano-service is reactive-re-deployed in quick succession. The first state
forces the transaction to commit and a reactive-re-deploy is scheduled
immediately. At that time the originating user may still be the owner of the
service instance because the service-owner kicker runs at the same time. It is
likely that re-deploy starts sooner than the kicker is able to change the owner.
To avoid this the service-owner includes the `/service-owner/nano-own` action
that can be triggered  as a *post-action* from the nano-service initial state.

An example of a plan-outline for a nano-service of this kind is included in
test-packages.

# License

This project is licensed under the MIT license. The Python package [erlang_py] is included in this project directly and is MIT licensed.

[erlang_py]: https://github.com/okeuday/erlang_py
