from typing import Iterable, List, Optional, Any
import threading
import dataclasses
import itertools
import ncs
import ncs.experimental
from . import erlang


DIFF_ITERATE_OP = {
    1: 'MOP_CREATED',
    2: 'MOP_DELETED',
    3: 'MOP_MODIFIED',
    4: 'MOP_VALUE_SET',
    5: 'MOP_MOVED_AFTER',
    6: 'MOP_ATTR_SET'
}


@dataclasses.dataclass
class ServiceOwner:
    username: str
    context: str


def _maybe_decode(instr: str | bytes) -> str:
    if isinstance(instr, bytes):
        return instr.decode('ascii')
    return instr


def _maybe_encode(instr: str) ->  str | bytes:
    # Encode to bytes for NSO < 6.4
    if ncs.LIB_API_VSN < 134479872:
        return instr.encode('ascii')
    else:
        return instr


def find_user_groups(trans: ncs.maapi.Transaction, user: str) -> Iterable[str]:
    with ncs.experimental.Query(trans,
                                expr=f"nacm:group[nacm:user-name='{user}']",
                                context_node='/nacm:nacm/nacm:groups',
                                select=['name'],
                                result_as=ncs.QUERY_STRING) as q:
       for group in q:
           yield group[0]


def own_service(trans: ncs.maapi.Transaction, service_kp: str, service_owner: ServiceOwner) -> Optional[List[Any]]:
    service = ncs.maagic.get_node(trans, service_kp)

    latest_u_info = service.private.latest_u_info
    if not isinstance(latest_u_info, bytes):
        raise ValueError(f'latest-u-info is not a bytes string: {latest_u_info}')

    read_term = erlang.binary_to_term(latest_u_info)

    if _maybe_decode(read_term[1].value) == service_owner.context \
    and _maybe_decode(read_term[3].value) == service_owner.username:
        return None

    # Convert the tuple of terms to a mutable list. We do this in
    # hopes of keeping this code forward-compatible - if new
    # terms are added the positions of existing remains fixed?
    write_term = list(read_term)
    # context
    write_term[1] = erlang.OtpErlangAtom(_maybe_encode(service_owner.context))
    # proto
    #write_term[2] = erlang.OtpErlangAtom('console'.encode('ascii'))
    # username
    write_term[3] = erlang.OtpErlangBinary(_maybe_encode(service_owner.username))
    # ip
    write_term[4] = (127,0,0,1)
    # port
    write_term[5] = 0
    # context
    if service_owner.context == 'system':
        write_term[8] = []
    else:
        write_term[8] = list(find_user_groups(trans, service_owner.username))
    new_u_info = erlang.term_to_binary(tuple(write_term))

    service.private.latest_u_info = new_u_info
    return write_term


class PostCommitDiffIterate:
    def __init__(self, log, service_owner: ServiceOwner):
        self.log = log
        self.service_owner = service_owner
        self.state: List[str] = []

    def __call__(self, keypath, operation, oldv, newv):
        self.log.debug(f'{keypath} [{DIFF_ITERATE_OP[operation]}]: {oldv} -> {newv}')
        # We are interested in MOP_SET_VALUE operations for the */private/latest-u-info leaf.
        # The state list contains the service instance keypaths where the leaf was modified.
        if operation == ncs.MOP_VALUE_SET \
           and str(keypath[0]) == 'latest-u-info' and str(keypath[1]) == 'private':
            self.state.append(str(keypath[2:]))
        return ncs.ITER_RECURSE

    def post_iterate(self):
        if not self.state:
            return

        commit_params = ncs.maapi.CommitParams()
        commit_params.no_deploy()
        with ncs.maapi.Maapi() as m, \
             ncs.maapi.Session(m, 'python-service-owner', 'system'):
            m.run_with_retry(self._retriable_iterate, commit_params=commit_params)

    def _retriable_iterate(self, t: ncs.maapi.Transaction) -> bool:
        commit = False
        for kp in self.state:
            self.log.debug(f'{kp}: start top-service processing')
            top_service = ncs.maagic.get_node(t, kp)
            for modified_service_kp in itertools.chain((kp,), top_service.modified.services):
                self.log.debug(f'{kp}: processing service {modified_service_kp}')
                try:
                    own_result = own_service(t, modified_service_kp, self.service_owner)
                except Exception as e:
                    self.log.error(f'{kp}: error updating latest-u-info for service {modified_service_kp}: {e}')
                else:
                    if own_result is not None:
                        self.log.debug(f'{kp}: updating latest-u-info for service {modified_service_kp}: {own_result}')
                        commit = True
        return commit


class TriggerAction(ncs.dp.Action):
    """Callback for the '/service-owner/trigger' action

    This action is executed by a kicker when the 'private/latest-u-info' leaf
    changes for any of the services listed in the entry-points of the profile.
    The action expects leaves from the 'kicker:action-input-params grouping be
    set. """
    @ncs.dp.Action.action
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        self.log.debug(f'{action_input.path}: post-commit kicked')

        with ncs.maapi.Maapi() as m:
            with ncs.maapi.Session(m, 'python-service-owner-own-action', 'system'):
                with m.start_read_trans() as t_read:
                    service = ncs.maagic.get_node(t_read, kp)
                    service_owner = ServiceOwner(service.username, service.context)
                diff_iterator = PostCommitDiffIterate(self.log, service_owner)
                t_synth = m.attach(action_input.tid)
                t_synth.diff_iterate(diff_iterator, 0)
                m.detach(action_input.tid)

        # After using the MAAPI session to attach to the synthetic transaction
        # it seems the user session gets messed up - cannot start new
        # transactions. Not sure if this is by design or is it a bug?? Anyway,
        # let's start a new MAAPI session in post_iterate
        diff_iterator.post_iterate()


class NanoOwnAction(ncs.dp.Action):
    """Callback for the /service-owner/nano-own action

    This action is executed from a nano-service as a post-action when the
    service instance needs to ensure it is owned by the user listed in the
    service-owner profile."""
    @ncs.dp.Action.action
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        self.log.debug(f'{kp}: nano-own action triggered')

        try:
            opaque_props = action_input.opaque_props
        except AttributeError:
            action_output.success = False
            action_output.message = 'nano-own action input must use the ncs:post-action-input-params grouping'
            return
        # opaque-props is an in-memory list so we have to iterate through it to find the key
        service_kp = None
        for prop in opaque_props:
            if prop.name == 'service_kp':
                service_kp = prop.value
        if service_kp is None:
            action_output.success = False
            action_output.message = 'nano-own action input must include the "service_kp" opaque property'
            return

        def _retriable_own(t: ncs.maapi.Transaction):
            self.log.debug(f'{service_kp}: processing service')
            so = ncs.maagic.get_node(t, kp)
            service_owner = ServiceOwner(so.username, so.context)
            own_result = own_service(t, service_kp, service_owner)
            if own_result is not None:
                self.log.debug(f'{service_kp}: updating latest-u-info: {own_result}')
                action_output.message = f'Updated latest-u-info to {own_result}'
                return True
            else:
                self.log.debug(f'{service_kp}: is already owned by {service_owner.username}')
                return False

        commit_params = ncs.maapi.CommitParams()
        commit_params.no_deploy()

        with ncs.maapi.Maapi() as m, \
             ncs.maapi.Session(m, 'python-service-owner-own-action', 'system'):
            m.run_with_retry(_retriable_own, commit_params=commit_params)

        action_output.success = True


class Main(ncs.application.Application):
    timer: threading.Timer

    def setup(self):
        self.register_action('service-owner-trigger-actionpoint', TriggerAction)
        self.register_action('service-owner-nano-own-actionpoint', NanoOwnAction)
        # re-deploy our service instances after the package is loaded to ensure kickers
        # exist for service instances craeted in init.xml
        self.timer = threading.Timer(20, self.re_deploy)
        self.timer.start()

    def teardown(self):
        self.timer.cancel()

    def re_deploy(self):
        with ncs.maapi.single_write_trans('python-service-owner-re-deploy', 'system') as t:
            root = ncs.maagic.get_root(t)
            for so in root.so__service_owner:
                self.log.debug(f'Touching {so._path}')
                so.touch()
            t.apply()
